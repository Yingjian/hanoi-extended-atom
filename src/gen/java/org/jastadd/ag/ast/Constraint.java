/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.ast:1
 * @astdecl Constraint : ASTNode ::= BinaryConnective* UnaryConnective* Atom*;
 * @production Constraint : {@link ASTNode} ::= <span class="component">{@link BinaryConnective}*</span> <span class="component">{@link UnaryConnective}*</span> <span class="component">{@link Atom}*</span>;

 */
public class Constraint extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect MoveTo
   * @declaredat E:\\project\\20211201\\src\\main\\jastadd\\hanoi\\MoveTo.jadd:2
   */
  public int check(Pillar P0, Pillar P1, boolean turn){
/*for every odd turn(turn==true), move the smallest disk D0 in sequence.
Therefore, CS checks for a move from P0 to P1, when D0 is on the top of P0.
Conjunction()-------------SD()---------R0()---------T0()
                      |
                      |---CS()---------R1()---------T1()
                                              |
                                              |-----T2()*/
    if(turn==true){
      Conjunction C=new Conjunction();
      Atom SD=new Atom();//if the smallest disk is on the pillar
      Atom CS=new Atom();//check if the move sequence from P0 to P1 is valid
      SelfEq R0= new SelfEq();
      Equal R1= new Equal();
      Term1 T0=new Term1();//return size of top disk, return -1 when no disk
      Term2 T1=new Term2();//return the target pillar's ID for the current pillar in odd turns
      Term3 T2=new Term3();//return the ID for the current pillar
      T0.setRel(P0);
      T1.setRel(P0);
      T2.setRel(P1);
      R0.setTerm(T0);
      R1.setLeft(T1);
      R1.setRight(T2);

      SD.setRelation(R0);
      CS.setRelation(R1);
      C.setLeft(SD);
      C.setRight(CS);
      this.addBinaryConnective(C);
      if(this.getBinaryConnective(0).eval()){
        return 1;//odd turn and valid move
      }else{
        return 0;
      }
    }
/*for every even turn(turn==false), move the disk D on top of P0 to P1:
D is not D0(the smallest disk).
There is always one D can be moved in the even turn according to the game rules.
Statements:
1.P0 is not empty: not OND().
2.When condition 1 holds, P1 can be empty: TND().
3.When 1 holds and 2 doesn\u9225\u6a9b hold, D0 < D1(for the disks on the top of P0 and P1 respectively).
Therefore, we want not OND() and (TND() or CS()) to be true.

Conjunction()-------------Negation()--------------OND()----------------R1()----------------T0()
                      |
                      |--------------------Disjunction()-----------TND()--------------R21()---------T1()
                                              |
                                              |------------------CS()-----------------R22()---------T0()
                                                                                        |---------T1*/
    Conjunction C0=new Conjunction();
    Negation C1=new Negation();
    Disjunction C2=new Disjunction();
    Atom OND=new Atom();//origin has no disk
    Atom TND=new Atom();//target has no disk
    Atom CS=new Atom();//check the size, if the disk on the top of origin has smaller size than the one on target

    C0.setLeft(C1);
    C0.setRight(C2);
    C1.setConnective(OND);
    C2.setLeft(TND);
    C2.setRight(CS);

    Term1 T0=new Term1();//return size of top disk, return -1 when no disk.
    Term1 T1=new Term1();

    T0.setRel(P0);
    T1.setRel(P1);

    SelfComp R1=new SelfComp();
    SelfComp R21=new SelfComp();
    Compare R22=new Compare();

    R1.setTerm(T0);
    R21.setTerm(T1);
    R22.setLeft(T0);
    R22.setRight(T1);

    OND.setRelation(R1);
    TND.setRelation(R21);
    CS.setRelation(R22);
    
    this.addBinaryConnective(C0);
    if(this.getBinaryConnective(0).eval()){
      return 2;//even turn and valid move
    }
    return 0;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:3
   */
  public static Constraint createRef(String ref) {
    Unresolved$Constraint unresolvedNode = new Unresolved$Constraint();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:9
   */
  public static Constraint createRefDirection(String ref) {
    Unresolved$Constraint unresolvedNode = new Unresolved$Constraint();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:300
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:419
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:425
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Constraint() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[3];  getChild_handler = new ASTNode$DepGraphNode[children.length];
    state().enterConstruction();
    setChild(new JastAddList(), 0);
    setChild(new JastAddList(), 1);
    setChild(new JastAddList(), 2);
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:18
   */
  @ASTNodeAnnotation.Constructor(
    name = {"BinaryConnective", "UnaryConnective", "Atom"},
    type = {"JastAddList<BinaryConnective>", "JastAddList<UnaryConnective>", "JastAddList<Atom>"},
    kind = {"List", "List", "List"}
  )
  public Constraint(JastAddList<BinaryConnective> p0, JastAddList<UnaryConnective> p1, JastAddList<Atom> p2) {
state().enterConstruction();
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:31
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 3;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:39
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:49
   */
  public Constraint clone() throws CloneNotSupportedException {
    Constraint node = (Constraint) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:54
   */
  public Constraint copy() {
    try {
      Constraint node = (Constraint) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:78
   */
  @Deprecated
  public Constraint fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:88
   */
  public Constraint treeCopyNoTransform() {
    Constraint tree = (Constraint) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:109
   */
  public Constraint treeCopy() {
    Constraint tree = (Constraint) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:124
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:127
   */
  protected void inc_copyHandlers(Constraint copy) {
    super.inc_copyHandlers(copy);

  }
  /** @apilevel internal 
   * @declaredat ASTNode:133
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:140
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:142
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:151
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:152
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:160
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:161
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   * Replaces the BinaryConnective list.
   * @param list The new list node to be used as the BinaryConnective list.
   * @apilevel high-level
   */
  public Constraint setBinaryConnectiveList(JastAddList<BinaryConnective> list) {
    setChild(list, 0);
    return this;
  }
  /**
   * Retrieves the number of children in the BinaryConnective list.
   * @return Number of children in the BinaryConnective list.
   * @apilevel high-level
   */
  public int getNumBinaryConnective() {
    return getBinaryConnectiveList().getNumChild();
  }
  /**
   * Retrieves the number of children in the BinaryConnective list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the BinaryConnective list.
   * @apilevel low-level
   */
  public int getNumBinaryConnectiveNoTransform() {
    return getBinaryConnectiveListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the BinaryConnective list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the BinaryConnective list.
   * @apilevel high-level
   */
  public BinaryConnective getBinaryConnective(int i) {
    return (BinaryConnective) getBinaryConnectiveList().getChild(i);
  }
  /**
   * Check whether the BinaryConnective list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasBinaryConnective() {
    return getBinaryConnectiveList().getNumChild() != 0;
  }
  /**
   * Append an element to the BinaryConnective list.
   * @param node The element to append to the BinaryConnective list.
   * @apilevel high-level
   */
  public Constraint addBinaryConnective(BinaryConnective node) {
    JastAddList<BinaryConnective> list = (parent == null) ? getBinaryConnectiveListNoTransform() : getBinaryConnectiveList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public Constraint addBinaryConnectiveNoTransform(BinaryConnective node) {
    JastAddList<BinaryConnective> list = getBinaryConnectiveListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the BinaryConnective list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public Constraint setBinaryConnective(BinaryConnective node, int i) {
    JastAddList<BinaryConnective> list = getBinaryConnectiveList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the BinaryConnective list.
   * @return The node representing the BinaryConnective list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="BinaryConnective")
  public JastAddList<BinaryConnective> getBinaryConnectiveList() {
    JastAddList<BinaryConnective> list = (JastAddList<BinaryConnective>) getChild(0);
    return list;
  }
  /**
   * Retrieves the BinaryConnective list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the BinaryConnective list.
   * @apilevel low-level
   */
  public JastAddList<BinaryConnective> getBinaryConnectiveListNoTransform() {
    return (JastAddList<BinaryConnective>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the BinaryConnective list without
   * triggering rewrites.
   */
  public BinaryConnective getBinaryConnectiveNoTransform(int i) {
    return (BinaryConnective) getBinaryConnectiveListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the BinaryConnective list.
   * @return The node representing the BinaryConnective list.
   * @apilevel high-level
   */
  public JastAddList<BinaryConnective> getBinaryConnectives() {
    return getBinaryConnectiveList();
  }
  /**
   * Retrieves the BinaryConnective list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the BinaryConnective list.
   * @apilevel low-level
   */
  public JastAddList<BinaryConnective> getBinaryConnectivesNoTransform() {
    return getBinaryConnectiveListNoTransform();
  }
  /**
   * Replaces the UnaryConnective list.
   * @param list The new list node to be used as the UnaryConnective list.
   * @apilevel high-level
   */
  public Constraint setUnaryConnectiveList(JastAddList<UnaryConnective> list) {
    setChild(list, 1);
    return this;
  }
  /**
   * Retrieves the number of children in the UnaryConnective list.
   * @return Number of children in the UnaryConnective list.
   * @apilevel high-level
   */
  public int getNumUnaryConnective() {
    return getUnaryConnectiveList().getNumChild();
  }
  /**
   * Retrieves the number of children in the UnaryConnective list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the UnaryConnective list.
   * @apilevel low-level
   */
  public int getNumUnaryConnectiveNoTransform() {
    return getUnaryConnectiveListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the UnaryConnective list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the UnaryConnective list.
   * @apilevel high-level
   */
  public UnaryConnective getUnaryConnective(int i) {
    return (UnaryConnective) getUnaryConnectiveList().getChild(i);
  }
  /**
   * Check whether the UnaryConnective list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasUnaryConnective() {
    return getUnaryConnectiveList().getNumChild() != 0;
  }
  /**
   * Append an element to the UnaryConnective list.
   * @param node The element to append to the UnaryConnective list.
   * @apilevel high-level
   */
  public Constraint addUnaryConnective(UnaryConnective node) {
    JastAddList<UnaryConnective> list = (parent == null) ? getUnaryConnectiveListNoTransform() : getUnaryConnectiveList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public Constraint addUnaryConnectiveNoTransform(UnaryConnective node) {
    JastAddList<UnaryConnective> list = getUnaryConnectiveListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the UnaryConnective list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public Constraint setUnaryConnective(UnaryConnective node, int i) {
    JastAddList<UnaryConnective> list = getUnaryConnectiveList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the UnaryConnective list.
   * @return The node representing the UnaryConnective list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="UnaryConnective")
  public JastAddList<UnaryConnective> getUnaryConnectiveList() {
    JastAddList<UnaryConnective> list = (JastAddList<UnaryConnective>) getChild(1);
    return list;
  }
  /**
   * Retrieves the UnaryConnective list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the UnaryConnective list.
   * @apilevel low-level
   */
  public JastAddList<UnaryConnective> getUnaryConnectiveListNoTransform() {
    return (JastAddList<UnaryConnective>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the UnaryConnective list without
   * triggering rewrites.
   */
  public UnaryConnective getUnaryConnectiveNoTransform(int i) {
    return (UnaryConnective) getUnaryConnectiveListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the UnaryConnective list.
   * @return The node representing the UnaryConnective list.
   * @apilevel high-level
   */
  public JastAddList<UnaryConnective> getUnaryConnectives() {
    return getUnaryConnectiveList();
  }
  /**
   * Retrieves the UnaryConnective list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the UnaryConnective list.
   * @apilevel low-level
   */
  public JastAddList<UnaryConnective> getUnaryConnectivesNoTransform() {
    return getUnaryConnectiveListNoTransform();
  }
  /**
   * Replaces the Atom list.
   * @param list The new list node to be used as the Atom list.
   * @apilevel high-level
   */
  public Constraint setAtomList(JastAddList<Atom> list) {
    setChild(list, 2);
    return this;
  }
  /**
   * Retrieves the number of children in the Atom list.
   * @return Number of children in the Atom list.
   * @apilevel high-level
   */
  public int getNumAtom() {
    return getAtomList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Atom list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Atom list.
   * @apilevel low-level
   */
  public int getNumAtomNoTransform() {
    return getAtomListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Atom list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Atom list.
   * @apilevel high-level
   */
  public Atom getAtom(int i) {
    return (Atom) getAtomList().getChild(i);
  }
  /**
   * Check whether the Atom list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasAtom() {
    return getAtomList().getNumChild() != 0;
  }
  /**
   * Append an element to the Atom list.
   * @param node The element to append to the Atom list.
   * @apilevel high-level
   */
  public Constraint addAtom(Atom node) {
    JastAddList<Atom> list = (parent == null) ? getAtomListNoTransform() : getAtomList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public Constraint addAtomNoTransform(Atom node) {
    JastAddList<Atom> list = getAtomListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the Atom list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public Constraint setAtom(Atom node, int i) {
    JastAddList<Atom> list = getAtomList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the Atom list.
   * @return The node representing the Atom list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Atom")
  public JastAddList<Atom> getAtomList() {
    JastAddList<Atom> list = (JastAddList<Atom>) getChild(2);
    return list;
  }
  /**
   * Retrieves the Atom list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Atom list.
   * @apilevel low-level
   */
  public JastAddList<Atom> getAtomListNoTransform() {
    return (JastAddList<Atom>) getChildNoTransform(2);
  }
  /**
   * @return the element at index {@code i} in the Atom list without
   * triggering rewrites.
   */
  public Atom getAtomNoTransform(int i) {
    return (Atom) getAtomListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Atom list.
   * @return The node representing the Atom list.
   * @apilevel high-level
   */
  public JastAddList<Atom> getAtoms() {
    return getAtomList();
  }
  /**
   * Retrieves the Atom list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Atom list.
   * @apilevel low-level
   */
  public JastAddList<Atom> getAtomsNoTransform() {
    return getAtomListNoTransform();
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
