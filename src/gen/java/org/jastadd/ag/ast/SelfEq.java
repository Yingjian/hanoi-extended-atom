/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.ast:10
 * @astdecl SelfEq : UnaryRelation ::= Term;
 * @production SelfEq : {@link UnaryRelation};

 */
public class SelfEq extends UnaryRelation implements Cloneable {
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:111
   */
  public static SelfEq createRef(String ref) {
    Unresolved$SelfEq unresolvedNode = new Unresolved$SelfEq();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:117
   */
  public static SelfEq createRefDirection(String ref) {
    Unresolved$SelfEq unresolvedNode = new Unresolved$SelfEq();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:336
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:671
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:677
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public SelfEq() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[1];  getChild_handler = new ASTNode$DepGraphNode[children.length];
    state().enterConstruction();
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:15
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Term"},
    type = {"Term"},
    kind = {"Child"}
  )
  public SelfEq(Term p0) {
state().enterConstruction();
    setChild(p0, 0);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:26
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:34
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:44
   */
  public SelfEq clone() throws CloneNotSupportedException {
    SelfEq node = (SelfEq) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:49
   */
  public SelfEq copy() {
    try {
      SelfEq node = (SelfEq) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:73
   */
  @Deprecated
  public SelfEq fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:83
   */
  public SelfEq treeCopyNoTransform() {
    SelfEq tree = (SelfEq) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:104
   */
  public SelfEq treeCopy() {
    SelfEq tree = (SelfEq) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:119
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:122
   */
  protected ASTNode$DepGraphNode eval_handler;
  /**
   * @declaredat ASTNode:123
   */
  protected void inc_copyHandlers(SelfEq copy) {
    super.inc_copyHandlers(copy);

        if (eval_handler != null) {
          copy.eval_handler = ASTNode$DepGraphNode.createAttrHandler(eval_handler, copy);
        }
  }
  /** @apilevel internal 
   * @declaredat ASTNode:132
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:139
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:141
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  if (eval_handler != null) {
    eval_handler.throwAway();
  }
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:153
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:154
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  if (eval_handler != null) {
    eval_handler.cleanupListeners();
  }
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:165
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:166
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   * Replaces the Term child.
   * @param node The new node to replace the Term child.
   * @apilevel high-level
   */
  public SelfEq setTerm(Term node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the Term child.
   * @return The current node used as the Term child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Term")
  public Term getTerm() {
    return (Term) getChild(0);
  }
  /**
   * Retrieves the Term child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Term child.
   * @apilevel low-level
   */
  public Term getTermNoTransform() {
    return (Term) getChildNoTransform(0);
  }
  /** @apilevel internal */
  private void eval_reset() {
    state().trace().flushAttr(this, "Relation.eval()", "", eval_value);
    eval_computed = null;
  }
  /** @apilevel internal */
  protected ASTState.Cycle eval_computed = null;

  /** @apilevel internal */
  protected boolean eval_value;

  /**
   * @attribute syn
   * @aspect Connectives
   * @declaredat E:\\project\\20211201\\src\\main\\jastadd\\hanoi\\Constraints.jrag:36
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Connectives", declaredAt="E:\\project\\20211201\\src\\main\\jastadd\\hanoi\\Constraints.jrag:17")
  public boolean eval() {
    ASTState state = state();
    
    if (eval_handler == null) {
      eval_handler = new ASTNode$DepGraphNode(this, "eval", null, ASTNode.inc_EMPTY) {
        @Override public void reactToDependencyChange() {
          {
            eval_computed = null;
            eval_handler.notifyDependencies();
            SelfEq.this.state().trace().flushIncAttr(SelfEq.this, "eval", "", "");
          }
        }
      };
    }
    state().addHandlerDepTo(eval_handler);
    
    
    
    
    
    if (eval_computed == ASTState.NON_CYCLE || eval_computed == state().cycle()) {
      state().trace().cacheRead(this, "Relation.eval()", "", eval_value);
      return eval_value;
    }
    
    state().enterAttrStoreEval(eval_handler);
    eval_value = eval_compute();
    if (state().inCircle()) {
      eval_computed = state().cycle();
      state().trace().cacheWrite(this, "Relation.eval()", "", eval_value);
    } else {
      eval_computed = ASTState.NON_CYCLE;
      state().trace().cacheWrite(this, "Relation.eval()", "", eval_value);
    }
    
    state().exitAttrStoreEval(eval_handler);
    
    
    
    
    
    
    return eval_value;
  }
  /** @apilevel internal */
  private boolean eval_compute() {//has smallest disk on top
          return this.getTerm().eval()==1;
      }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
