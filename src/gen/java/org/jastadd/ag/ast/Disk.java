/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.ast:22
 * @astdecl Disk : ASTNode ::= <Size:int>;
 * @production Disk : {@link ASTNode} ::= <span class="component">&lt;Size:{@link int}&gt;</span>;

 */
public class Disk extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:255
   */
  public static Disk createRef(String ref) {
    Unresolved$Disk unresolvedNode = new Unresolved$Disk();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:261
   */
  public static Disk createRefDirection(String ref) {
    Unresolved$Disk unresolvedNode = new Unresolved$Disk();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:387
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:1007
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:1013
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Disk() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    state().enterConstruction();
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Size"},
    type = {"int"},
    kind = {"Token"}
  )
  public Disk(int p0) {
state().enterConstruction();
    setSize(p0);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:25
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:33
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public Disk clone() throws CloneNotSupportedException {
    Disk node = (Disk) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:48
   */
  public Disk copy() {
    try {
      Disk node = (Disk) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:72
   */
  @Deprecated
  public Disk fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:82
   */
  public Disk treeCopyNoTransform() {
    Disk tree = (Disk) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:103
   */
  public Disk treeCopy() {
    Disk tree = (Disk) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:118
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:121
   */
  protected java.util.Map smallerThan_Disk_handler = new java.util.HashMap(4);
  /**
   * @declaredat ASTNode:122
   */
  protected void inc_copyHandlers(Disk copy) {
    super.inc_copyHandlers(copy);

        if (getSize_handler != null) {
          copy.getSize_handler = ASTNode$DepGraphNode.createAstHandler(getSize_handler, copy);
        }
        if (smallerThan_Disk_handler != null) {
          copy.smallerThan_Disk_handler = new java.util.HashMap(4);
        }
  }
  /** @apilevel internal 
   * @declaredat ASTNode:134
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:141
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:143
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  if (getSize_handler != null) {
    getSize_handler.throwAway();
  }
  for (java.util.Iterator itr = smallerThan_Disk_handler.values().iterator(); itr.hasNext();) {
    ASTNode$DepGraphNode handler = (ASTNode$DepGraphNode) itr.next();
    handler.throwAway();
  }
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:159
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:160
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  if (getSize_handler != null) {
    getSize_handler.cleanupListeners();
  }
  for (java.util.Iterator itr = smallerThan_Disk_handler.values().iterator(); itr.hasNext();) {
    ASTNode$DepGraphNode handler = (ASTNode$DepGraphNode)itr.next();
    handler.cleanupListeners();
  }
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:175
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:176
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   */
  protected ASTNode$DepGraphNode getSize_handler = ASTNode$DepGraphNode.createAstHandler(this, "getSize", null);
  /**
   * Replaces the lexeme Size.
   * @param value The new value for the lexeme Size.
   * @apilevel high-level
   */
  public Disk setSize(int value) {
    tokenint_Size = value;
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      getSize_handler.notifyDependencies();
    
    
    
    
    }
    return this;
  }
  /** @apilevel internal 
   */
  protected int tokenint_Size;
  /**
   * Retrieves the value for the lexeme Size.
   * @return The value for the lexeme Size.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Size")
  public int getSize() {
    
    state().addHandlerDepTo(getSize_handler);
    return tokenint_Size;
  }
  /** @apilevel internal */
  private void smallerThan_Disk_reset() {
    state().trace().flushAttr(this, "Disk.smallerThan(Disk)", "", smallerThan_Disk_values);
    smallerThan_Disk_computed = null;
    smallerThan_Disk_values = null;
  }
  /** @apilevel internal */
  protected java.util.Map smallerThan_Disk_values;
  /** @apilevel internal */
  protected java.util.Map smallerThan_Disk_computed;
  /**
   * @attribute syn
   * @aspect CanMove
   * @declaredat E:\\project\\20211201\\src\\main\\jastadd\\hanoi\\CanMove.jrag:2
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CanMove", declaredAt="E:\\project\\20211201\\src\\main\\jastadd\\hanoi\\CanMove.jrag:2")
  public boolean smallerThan(Disk D) {
    Object _parameters = D;
    if (smallerThan_Disk_computed == null) smallerThan_Disk_computed = new java.util.HashMap(4);
    if (smallerThan_Disk_values == null) smallerThan_Disk_values = new java.util.HashMap(4);
    ASTState state = state();
    
    if (!smallerThan_Disk_handler.containsKey(_parameters)) {
      smallerThan_Disk_handler.put(_parameters, new ASTNode$DepGraphNode(this, "smallerThan_Disk", _parameters, ASTNode.inc_EMPTY) {
        @Override public void reactToDependencyChange() {
          if (smallerThan_Disk_values != null && smallerThan_Disk_values.containsKey(fParams)) {
            smallerThan_Disk_values.remove(fParams);
            ASTNode$DepGraphNode handler = (ASTNode$DepGraphNode) smallerThan_Disk_handler.remove(fParams);
            handler.throwAway();
            handler.notifyDependencies();
          }
        }
      });
    }
    state().addHandlerDepTo((ASTNode$DepGraphNode)smallerThan_Disk_handler.get(_parameters));
    
    
    
    
    
    if (smallerThan_Disk_values.containsKey(_parameters)
        && smallerThan_Disk_computed.containsKey(_parameters)
        && (smallerThan_Disk_computed.get(_parameters) == ASTState.NON_CYCLE || smallerThan_Disk_computed.get(_parameters) == state().cycle())) {
      state().trace().cacheRead(this, "Disk.smallerThan(Disk)", _parameters, smallerThan_Disk_values.get(_parameters));
      return (Boolean) smallerThan_Disk_values.get(_parameters);
    }
    
    state().enterAttrStoreEval((ASTNode$DepGraphNode)smallerThan_Disk_handler.get(_parameters));
    boolean smallerThan_Disk_value = smallerThan_compute(D);
    if (state().inCircle()) {
      smallerThan_Disk_values.put(_parameters, smallerThan_Disk_value);
      smallerThan_Disk_computed.put(_parameters, state().cycle());
      state().trace().cacheWrite(this, "Disk.smallerThan(Disk)", _parameters, smallerThan_Disk_value);
    } else {
      smallerThan_Disk_values.put(_parameters, smallerThan_Disk_value);
      smallerThan_Disk_computed.put(_parameters, ASTState.NON_CYCLE);
      state().trace().cacheWrite(this, "Disk.smallerThan(Disk)", _parameters, smallerThan_Disk_value);
    }
    
    state().exitAttrStoreEval((ASTNode$DepGraphNode)smallerThan_Disk_handler.get(_parameters));
    
    
    
    
    
    
    return smallerThan_Disk_value;
  }
  /** @apilevel internal */
  private boolean smallerThan_compute(Disk D) {
      if(this.getSize() < D.getSize()){
        return true;
      }
        return false;
    }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
