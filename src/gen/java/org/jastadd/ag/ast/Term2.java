/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.ast:19
 * @astdecl Term2 : Term ::= <_impl_Rel:Pillar>;
 * @production Term2 : {@link Term} ::= <span class="component">&lt;_impl_Rel:{@link Pillar}&gt;</span>;

 */
public class Term2 extends Term implements Cloneable {
  /**
   * @aspect Connectives
   * @declaredat E:\\project\\20211201\\src\\main\\jastadd\\hanoi\\Constraints.jrag:56
   */
  public int eval(){
        return this.getRel().moveSeq();
    }
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.jadd:22
   */
  public Pillar getRel() {
    if (tokenPillar__impl_Rel != null && tokenPillar__impl_Rel.is$Unresolved()) {
      if (tokenPillar__impl_Rel.as$Unresolved().getUnresolved$ResolveOpposite()) {
        setRel(resolveRelByToken(tokenPillar__impl_Rel.as$Unresolved().getUnresolved$Token()));
      } else {
        set_impl_Rel(resolveRelByToken(tokenPillar__impl_Rel.as$Unresolved().getUnresolved$Token()));
      }
    }
    return get_impl_Rel();
  }
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.jadd:32
   */
  public Term2 setRel(Pillar o) {
    assertNotNull(o);
    set_impl_Rel(o);
    return this;
  }
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.jadd:75
   */
  public void computeLowerBoundsViolations(java.util.List<Pair<ASTNode, String>> list) {
    if (getRel() == null) {
      list.add(new Pair<>(this, "Rel"));
    }
    super.computeLowerBoundsViolations(list);
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:219
   */
  public static Term2 createRef(String ref) {
    Unresolved$Term2 unresolvedNode = new Unresolved$Term2();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:225
   */
  public static Term2 createRefDirection(String ref) {
    Unresolved$Term2 unresolvedNode = new Unresolved$Term2();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:373
   */
  public void resolveAll() {
    getRel();
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:923
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:929
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Term2() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    state().enterConstruction();
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"_impl_Rel"},
    type = {"Pillar"},
    kind = {"Token"}
  )
  public Term2(Pillar p0) {
state().enterConstruction();
    set_impl_Rel(p0);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:25
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:33
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public Term2 clone() throws CloneNotSupportedException {
    Term2 node = (Term2) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:48
   */
  public Term2 copy() {
    try {
      Term2 node = (Term2) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:72
   */
  @Deprecated
  public Term2 fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:82
   */
  public Term2 treeCopyNoTransform() {
    Term2 tree = (Term2) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:103
   */
  public Term2 treeCopy() {
    Term2 tree = (Term2) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:118
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:121
   */
  protected void inc_copyHandlers(Term2 copy) {
    super.inc_copyHandlers(copy);

        if (get_impl_Rel_handler != null) {
          copy.get_impl_Rel_handler = ASTNode$DepGraphNode.createAstHandler(get_impl_Rel_handler, copy);
        }
  }
  /** @apilevel internal 
   * @declaredat ASTNode:130
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:137
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:139
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  if (get_impl_Rel_handler != null) {
    get_impl_Rel_handler.throwAway();
  }
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:151
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:152
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  if (get_impl_Rel_handler != null) {
    get_impl_Rel_handler.cleanupListeners();
  }
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:163
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:164
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   */
  protected ASTNode$DepGraphNode get_impl_Rel_handler = ASTNode$DepGraphNode.createAstHandler(this, "get_impl_Rel", null);
  /**
   * Replaces the lexeme _impl_Rel.
   * @param value The new value for the lexeme _impl_Rel.
   * @apilevel high-level
   */
  public Term2 set_impl_Rel(Pillar value) {
    tokenPillar__impl_Rel = value;
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      get_impl_Rel_handler.notifyDependencies();
    
    
    
    
    }
    return this;
  }
  /** @apilevel internal 
   */
  protected Pillar tokenPillar__impl_Rel;
  /**
   * Retrieves the value for the lexeme _impl_Rel.
   * @return The value for the lexeme _impl_Rel.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="_impl_Rel")
  public Pillar get_impl_Rel() {
    
    state().addHandlerDepTo(get_impl_Rel_handler);
    return tokenPillar__impl_Rel;
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:14
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:14")
  public Pillar resolveRelByToken(String id) {
    {
        // default to context-independent name resolution
        return globallyResolvePillarByToken(id);
      }
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
