package org.jastadd.ag.ast;

import java.util.*;
/**
 * @ast class
 * @aspect RefResolverHelpers
 * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:515
 */
abstract class Unresolved$Relation extends Relation implements Unresolved$Node$Interface {
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:516
   */
  
    private String unresolved$Token;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:517
   */
  
    public String getUnresolved$Token() {
      return unresolved$Token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:520
   */
  
    void setUnresolved$Token(String token) {
      this.unresolved$Token = token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:523
   */
  
    private boolean unresolved$ResolveOpposite;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:524
   */
  
    public boolean getUnresolved$ResolveOpposite() {
      return unresolved$ResolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:527
   */
  
    void setUnresolved$ResolveOpposite(boolean resolveOpposite) {
      this.unresolved$ResolveOpposite = resolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:534
   */
  Unresolved$Node$Interface as$Unresolved() {
    return this;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:540
   */
  boolean is$Unresolved() {
    return true;
  }

}
