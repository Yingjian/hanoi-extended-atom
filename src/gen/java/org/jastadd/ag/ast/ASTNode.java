/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @astdecl ASTNode;
 * @production ASTNode;

 */
public class ASTNode<T extends ASTNode> implements Cloneable {
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.jadd:56
   */
  public boolean violatesLowerBounds() {
    return !getLowerBoundsViolations().isEmpty();
  }
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.jadd:59
   */
  public java.util.List<Pair<ASTNode, String>> getLowerBoundsViolations() {
    java.util.List<Pair<ASTNode, String>> list = new java.util.ArrayList<>();
    computeLowerBoundsViolations(list);
    return list;
  }
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.jadd:64
   */
  public void computeLowerBoundsViolations(java.util.List<Pair<ASTNode, String>> list) {
    for (int i = 0; i < getNumChildNoTransform(); i++) {
      getChildNoTransform(i).computeLowerBoundsViolations(list);
    }
  }
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.jadd:108
   */
  public static void assertNotNull(Object obj) {
    if (obj == null) {
      throw new NullPointerException();
    }
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:284
   */
  public void resolveAll() {
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:288
   */
  public void treeResolveAll() {
    if (children != null) {
      for (int i = 0; i < numChildren; ++i) {
        ASTNode child = children[i];
        if (child != null) {
          child.treeResolveAll();
        }
      }
    }
    resolveAll();
  }
  /**
   * @declaredat ASTNode:1
   */
  public ASTNode() {
    super();
    init$Children();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:11
   */
  public void init$Children() {
    state().enterConstruction();
    state().exitConstruction();
  }
  /**
   * Cached child index. Child indices are assumed to never change (AST should
   * not change after construction).
   * @apilevel internal
   * @declaredat ASTNode:20
   */
  private int childIndex = -1;
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  public int getIndexOfChild(ASTNode node) {
    if (node == null) {
      return -1;
    }
    if (node.childIndex >= 0) {
      return node.childIndex;
    }
    for (int i = 0; children != null && i < children.length; i++) {
      if (getChild(i) == node) {
        node.childIndex = i;
        return i;
      }
    }
    return -1;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  public static final boolean generatedWithCacheCycle = false;
  /** @apilevel low-level 
   * @declaredat ASTNode:43
   */
  protected ASTNode parent;
  /** @apilevel low-level 
   * @declaredat ASTNode:46
   */
  protected ASTNode[] children;
  /**
   * @declaredat ASTNode:48
   */
  public final ASTState.Trace trace() {
    return state().trace();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:53
   */
  private static ASTState state = new ASTState();
  /** @apilevel internal 
   * @declaredat ASTNode:56
   */
  public final ASTState state() {
    return state;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:61
   */
  public final static ASTState resetState() {
    return state = new ASTState();
  }
  /**
   * @return an iterator that can be used to iterate over the children of this node.
   * The iterator does not allow removing children.
   * @declaredat ASTNode:70
   */
  public java.util.Iterator<T> astChildIterator() {
    return new java.util.Iterator<T>() {
      private int index = 0;

      @Override
      public boolean hasNext() {
        return index < getNumChild();
      }

      @Override
      public T next() {
        return hasNext() ? (T) getChild(index++) : null;
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException();
      }
    };
  }
  /** @return an object that can be used to iterate over the children of this node 
   * @declaredat ASTNode:92
   */
  public Iterable<T> astChildren() {
    return new Iterable<T>() {
      @Override
      public java.util.Iterator<T> iterator() {
        return astChildIterator();
      }
    };
  }
  /**
   * @declaredat ASTNode:101
   */
  public static String nodeToString(Object node) {
    return (node != null ? node.getClass().getSimpleName() : "null");
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:105
   */
  public T getChild(int i) {
    ASTNode node = this.getChildNoTransform(i);
    
    if (getChild_handler[i] == null) {
      getChild_handler[i] = ASTNode$DepGraphNode.createAttrHandler(this, "getChild", Integer.valueOf(i));
    }
    state().addHandlerDepTo(getChild_handler[i]);
    if (node != null && node.mayHaveRewrite()) {
      
      state().enterAttrStoreEval(getChild_handler[i]);
      ASTNode rewritten = node.rewrittenNode();
      if (rewritten != node) {
        state().enterConstruction();
        rewritten.setParent(this);
        state().exitConstruction();
        node = rewritten;
      }
      
      state().exitAttrStoreEval(getChild_handler[i]);
    }
    return (T) node;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:128
   */
  public ASTNode addChild(T node) {
    setChild(node, getNumChildNoTransform());
    int i = getNumChildNoTransform() - 1;
    if (getChild_handler[i] == null) {
      getChild_handler[i] = ASTNode$DepGraphNode.createAttrHandler(this, "getChild", Integer.valueOf(i));
    }
    state().addHandlerDepTo(getChild_handler[i]);
    return this;
  }
  /**
   * Gets a child without triggering rewrites.
   * @apilevel low-level
   * @declaredat ASTNode:141
   */
  public T getChildNoTransform(int i) {
    if (children == null) {
      return null;
    }
    T child = (T) children[i];
    return child;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:149
   */
  protected int numChildren;
  /** @apilevel low-level 
   * @declaredat ASTNode:152
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return numChildren;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:159
   */
  public int getNumChild() {
    return numChildren();
  }
  /**
   * Behaves like getNumChild, but does not invoke AST transformations (rewrites).
   * @apilevel low-level
   * @declaredat ASTNode:167
   */
  public final int getNumChildNoTransform() {
    return numChildren();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:171
   */
  public ASTNode setChild(ASTNode node, int i) {
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      if (children != null && i < children.length && children[i] != null) {
        children[i].inc_notifyForRemove();
      }
      if (children == null) {
        numChildren_handler.notifyDependencies();
      } else if (i >= numChildren) {
        numChildren_handler.notifyDependencies();
        if (i > 0 && getChild_handler[i-1] != null) {
          getChild_handler[i-1].notifyDependencies();
        }
      } else {
        if (getChild_handler[i] != null) {
          getChild_handler[i].notifyDependencies();
        } else {
          getChild_handler[i] = ASTNode$DepGraphNode.createAttrHandler(this, "getChild", Integer.valueOf(i));
        }
      }
    
    
    
    
    }
    if (children == null) {
      children = new ASTNode[(i + 1 > 4 || !(this instanceof JastAddList)) ? i + 1 : 4];
      
      getChild_handler = new ASTNode$DepGraphNode[(i + 1 > 4 || !(this instanceof JastAddList)) ? i + 1 : 4];
    } else if (i >= children.length) {
      ASTNode c[] = new ASTNode[i << 1];
      System.arraycopy(children, 0, c, 0, children.length);
      children = c;
      
      if (getChild_handler != null) {
        ASTNode$DepGraphNode h[] = new ASTNode$DepGraphNode[i << 1];
        System.arraycopy(getChild_handler, 0, h, 0, getChild_handler.length);
        getChild_handler = h;
      }
    }
    
    if (children[i] != null) {
      children[i].inc_throwAway();
      children[i].parent = null;
    }
    children[i] = node;
    if (i >= numChildren) {
      numChildren = i+1;
    }
    if (node != null) {
      node.setParent(this);
      node.childIndex = i;
    }
    return this;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:227
   */
  public ASTNode insertChild(ASTNode node, int i) {
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      numChildren_handler.notifyDependencies();
      if (children != null && i <= numChildren) {
        for (int k = i; k < children.length; k++) {
          if (getChild_handler[k] != null) {
            getChild_handler[k].notifyDependencies();
          }
        }
      }
    
    
    
    
    }
    if (children == null) {
      children = new ASTNode[(i + 1 > 4 || !(this instanceof JastAddList)) ? i + 1 : 4];
      children[i] = node;
      
      getChild_handler = new ASTNode$DepGraphNode[(i + 1 > 4 || !(this instanceof JastAddList)) ? i + 1 : 4];
      getChild_handler[i] = ASTNode$DepGraphNode.createAttrHandler(this, "getChild", Integer.valueOf(i));
    } else {
      ASTNode c[] = new ASTNode[children.length + 1];
      System.arraycopy(children, 0, c, 0, i);
      c[i] = node;
      if (i < children.length) {
        System.arraycopy(children, i, c, i+1, children.length-i);
        for(int j = i+1; j < c.length; ++j) {
          if (c[j] != null) {
            c[j].childIndex = j;
          }
        }
      }
      children = c;
      
      if (getChild_handler != null) {
        ASTNode$DepGraphNode h[] = new ASTNode$DepGraphNode[getChild_handler.length + 1];
        System.arraycopy(getChild_handler, 0, h, 0, getChild_handler.length);
        getChild_handler = h;
      }
    }
    numChildren++;
    if (node != null) {
      node.setParent(this);
      node.childIndex = i;
    }
    return this;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:277
   */
  public void removeChild(int i) {
    if (children != null) {
      
      if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
        if (children[i] != null) {
          children[i].inc_notifyForRemove();
        }
        numChildren_handler.notifyDependencies();
        for (int k = i; k < children.length; k++) {
          if (getChild_handler[k] != null) {
            getChild_handler[k].notifyDependencies();
          }
        }
      
      
      
      
      }
      ASTNode child = (ASTNode) children[i];
      if (child != null) {
        
        // Prevent recursive call during state handling where setParent calls removeChild.
        child.inc_throwAway();
        child.parent = null;
        child.childIndex = -1;
      }
      // Adding a check of this instance to make sure its a List, a move of children doesn't make
      // any sense for a node unless its a list. Also, there is a problem if a child of a non-List node is removed
      // and siblings are moved one step to the right, with null at the end.
      if (this instanceof JastAddList || this instanceof Opt) {
        System.arraycopy(children, i+1, children, i, children.length-i-1);
        children[children.length-1] = null;
        numChildren--;
        // fix child indices
        for(int j = i; j < numChildren; ++j) {
          if (children[j] != null) {
            child = (ASTNode) children[j];
            child.childIndex = j;
          }
        }
      } else {
        children[i] = null;
      }
      
      if (getChild_handler != null && this instanceof JastAddList) {
        getChild_handler[numChildren] = null;
      }
    }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:327
   */
  public ASTNode getParent() {
    
    state().addHandlerDepTo(getParent_handler);
    return (ASTNode) parent;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:333
   */
  public void setParent(ASTNode node) {
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      getParent_handler.notifyDependencies();
    }
    /*
    if (node != null) {
      inc_changeState(node.inc_state);
    } else {
      inc_changeState(inc_GARBAGE);
    }
    */
    parent = node;
  }
  /**
   * Line and column information.
   * @declaredat ASTNode:405
   */
  protected int startLine;
  /**
   * @declaredat ASTNode:406
   */
  protected short startColumn;
  /**
   * @declaredat ASTNode:407
   */
  protected int endLine;
  /**
   * @declaredat ASTNode:408
   */
  protected short endColumn;
  /**
   * @declaredat ASTNode:410
   */
  public int getStartLine() {
    return startLine;
  }
  /**
   * @declaredat ASTNode:413
   */
  public short getStartColumn() {
    return startColumn;
  }
  /**
   * @declaredat ASTNode:416
   */
  public int getEndLine() {
    return endLine;
  }
  /**
   * @declaredat ASTNode:419
   */
  public short getEndColumn() {
    return endColumn;
  }
  /**
   * @declaredat ASTNode:423
   */
  public void setStart(int startLine, short startColumn) {
    this.startLine = startLine;
    this.startColumn = startColumn;
  }
  /**
   * @declaredat ASTNode:427
   */
  public void setEnd(int endLine, short endColumn) {
    this.endLine = endLine;
    this.endColumn = endColumn;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:439
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:443
   */
  public void flushTreeCache() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:446
   */
  public void flushCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:449
   */
  public void flushAttrAndCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:452
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:455
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:458
   */
  public ASTNode<T> clone() throws CloneNotSupportedException {
    ASTNode node = (ASTNode) super.clone();
    node.flushAttrAndCollectionCache();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:464
   */
  public ASTNode<T> copy() {
    try {
      ASTNode node = (ASTNode) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:488
   */
  @Deprecated
  public ASTNode<T> fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:498
   */
  public ASTNode<T> treeCopyNoTransform() {
    ASTNode tree = (ASTNode) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:519
   */
  public ASTNode<T> treeCopy() {
    ASTNode tree = (ASTNode) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Performs a full traversal of the tree using getChild to trigger rewrites
   * @apilevel low-level
   * @declaredat ASTNode:537
   */
  public void doFullTraversal() {
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).doFullTraversal();
    }
  }
  /**
   * @declaredat ASTNode:542
   */
  public String relativeNodeID() {
  StringBuffer res = new StringBuffer();
  ASTNode parent = this.parent;
  int index = -1;
  if (parent != null) {
    res.append(parent.relativeNodeID() + "/");
    for (int i = 0; parent.children != null && i < parent.children.length; i++) {
      if (parent.children[i] != null && parent.children[i] == this && !parent.childIsNTA(i)) {
        index = i;
        break;
      }
    }
  }
  res.append(getClass().getSimpleName());
  if (index > -1) {
    res.append("[" + index + (mayHaveRewrite() ? ",r" : "") + "]");
  }
  return res.toString();
}
  /** @apilevel internal 
   * @declaredat ASTNode:562
   */
  protected boolean childIsNTA(int index) {
    return false;
  }
  /**
   * @declaredat ASTNode:565
   */
  protected ASTNode$DepGraphNode getParent_handler = ASTNode$DepGraphNode.createAstHandler(this, "getParent", null);
  /**
   * @declaredat ASTNode:566
   */
  protected ASTNode$DepGraphNode numChildren_handler = ASTNode$DepGraphNode.createAstHandler(this, "numChildren", null);
  /**
   * @declaredat ASTNode:567
   */
  protected ASTNode$DepGraphNode[] getChild_handler;
  /**
   * @declaredat ASTNode:568
   */
  protected void inc_copyHandlers(ASTNode copy) {
    // ast handlers
    if (getChild_handler != null) {
      copy.getChild_handler = (ASTNode$DepGraphNode[])getChild_handler.clone();
    }
    copy.numChildren_handler = ASTNode$DepGraphNode.createAstHandler(numChildren_handler, copy);
    copy.getParent_handler = ASTNode$DepGraphNode.createAstHandler(getParent_handler, copy);
    for (int i = 0; getChild_handler != null && i < getChild_handler.length; i++) {
      if (getChild_handler[i] != null) {
        copy.getChild_handler[i] = ASTNode$DepGraphNode.createAttrHandler(getChild_handler[i], copy);
      }
    }

  }
  /** @apilevel internal 
   * @declaredat ASTNode:584
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    
    // flush children
    if (attrID.equals("getChild")) {
      int i = (Integer)_parameters;
      getChild_handler[i].notifyDependencies();
    }
  }
  /** @apilevel internal 
   * @declaredat ASTNode:601
   */
  public void inc_notifyForRemove() {
    getParent_handler.notifyDependencies();
  }
  /**
   * @declaredat ASTNode:604
   */
  public static final int inc_CREATED = 0;
  /**
   * @declaredat ASTNode:605
   */
  public static final int inc_CLONED = 1;
  /**
   * @declaredat ASTNode:606
   */
  public static final int inc_LIVE = 2;
  /**
   * @declaredat ASTNode:607
   */
  public static final int inc_GARBAGE = 3;
  /**
   * @declaredat ASTNode:608
   */
  public static final int inc_EMPTY = 4;
  /**
   * @declaredat ASTNode:609
   */
  public static final int inc_COMPUTED = 5;
  /**
   * @declaredat ASTNode:610
   */
  public static final int inc_AST = 6;
  /**
   * @declaredat ASTNode:611
   */
  public int inc_state = inc_CREATED;
  /**
   * @declaredat ASTNode:612
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:614
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  getParent_handler.throwAway();
  numChildren_handler.throwAway();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (getChild_handler[i] != null) {
      getChild_handler[i].throwAway();
    }
    if (child != null) {
      child.inc_throwAway();
    }
  }

  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:634
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:635
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  getParent_handler.cleanupListeners();
  numChildren_handler.cleanupListeners();
  for (int k = 0; getChild_handler != null && k < getChild_handler.length; k++) {
    if (getChild_handler[k] != null) {
      getChild_handler[k].cleanupListeners();
    }
  }
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:649
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:650
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:29
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:29")
  public Constraint globallyResolveConstraintByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for Constraint not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:36
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:36")
  public Term globallyResolveTermByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for Term not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:43
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:43")
  public Connective globallyResolveConnectiveByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for Connective not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:50
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:50")
  public Atom globallyResolveAtomByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for Atom not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:57
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:57")
  public Relation globallyResolveRelationByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for Relation not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:64
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:64")
  public UnaryRelation globallyResolveUnaryRelationByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for UnaryRelation not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:71
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:71")
  public BinaryRelation globallyResolveBinaryRelationByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for BinaryRelation not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:78
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:78")
  public Compare globallyResolveCompareByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for Compare not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:85
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:85")
  public Equal globallyResolveEqualByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for Equal not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:92
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:92")
  public SelfEq globallyResolveSelfEqByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for SelfEq not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:99
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:99")
  public SelfComp globallyResolveSelfCompByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for SelfComp not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:106
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:106")
  public UnaryConnective globallyResolveUnaryConnectiveByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for UnaryConnective not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:113
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:113")
  public BinaryConnective globallyResolveBinaryConnectiveByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for BinaryConnective not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:120
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:120")
  public Conjunction globallyResolveConjunctionByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for Conjunction not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:127
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:127")
  public Disjunction globallyResolveDisjunctionByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for Disjunction not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:134
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:134")
  public Implication globallyResolveImplicationByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for Implication not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:141
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:141")
  public Negation globallyResolveNegationByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for Negation not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:148
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:148")
  public Term1 globallyResolveTerm1ByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for Term1 not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:155
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:155")
  public Term2 globallyResolveTerm2ByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for Term2 not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:162
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:162")
  public Term3 globallyResolveTerm3ByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for Term3 not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:169
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:169")
  public Hanoi globallyResolveHanoiByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for Hanoi not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:176
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:176")
  public Disk globallyResolveDiskByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for Disk not implemented.");
      }
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:183
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:183")
  public Pillar globallyResolvePillarByToken(String id) {
    {
        // perform context independent name resolution here using the id
        throw new RuntimeException("Context-independent name resolution for Pillar not implemented.");
      }
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return this;
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }
  /** @apilevel internal */
  public int Define_moveSeq(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_moveSeq(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_moveSeq(self, _callerNode);
  }

  /**
   * @declaredat E:\\project\\20211201\\src\\main\\jastadd\\hanoi\\CanMove.jrag:13
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute moveSeq
   */
  protected boolean canDefine_moveSeq(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public int Define_ID(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_ID(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_ID(self, _callerNode);
  }

  /**
   * @declaredat E:\\project\\20211201\\src\\main\\jastadd\\hanoi\\CanMove.jrag:21
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute ID
   */
  protected boolean canDefine_ID(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
public ASTNode rewrittenNode() { throw new Error("rewrittenNode is undefined for ASTNode"); }

}
