```plantuml
@startuml
hide circle
hide methods

class EndTask
class DeviceOfComponent {
  Type
}
class JointOfComponent
class Connector {
  Translation
}
class Robot {
  BasePosition
  Name
  Type
  Role
  Id
}
class EndEffector {
  InitialPosition
  InitialOrientation
}
abstract class Process {
  Id
}
class Link
class Place {
  Place
}
class StartTask {
  CurrentTask
}
class EndProcess
abstract class ComponentProperty {
  Name
}
class RobotModel
class Movement {
  GoalPose
}
class Grasp {
  Grasp
}
class Physic {
  CenterOfMass
  Weight
}
class StartProcess
class WorkProcess {
  CurrentTask
}
class Component {
  Id
  IsConnected
}
abstract class Property {
  Name
}
class Device {
  Type
}
abstract class Subtask {
  Id
}
abstract class Behavior {
  Id
}
class ActionModel
class ComponentModel
class Condition {
  TypeOfComponent
  TypeOfNextComponent
  Position
  ConnectionSide
  BoundRange
}
class ContainerModel
class LinkOfComponent
class WorkFlowTask {
  IsSucceeded
  CurrentBehavior
  WorkPose
}
class Joint {
  MaxRange
  MaxVelocity
  MaxEffort
  MinRange
  MinVelocity
  MinEffort
}

Robot *-- "*" Property 
RobotModel *-- "*" Robot 
WorkProcess *-- "*" Subtask 
Component *-- "*" ComponentProperty 
Component *-- "1" Condition 
ActionModel *-- "*" Process 
ComponentModel *-- "*" Component 
ContainerModel *-- "1" RobotModel 
ContainerModel *-- "1" ActionModel 
ContainerModel *-- "1" ComponentModel 
WorkFlowTask *-- "*" Behavior 

EndProcess "1" <--> "1" Component 
StartProcess  --> "1" ComponentModel : GoalComponentModel
StartTask  --> "1" RobotModel : LinkRobot

Subtask <|-- EndTask
ComponentProperty <|-- DeviceOfComponent
ComponentProperty <|-- JointOfComponent
LinkOfComponent <|-- Connector
Joint <|-- EndEffector
Property <|-- Link
Behavior <|-- Place
Subtask <|-- StartTask
Process <|-- EndProcess
Behavior <|-- Movement
Behavior <|-- Grasp
ComponentProperty <|-- Physic
Process <|-- StartProcess
Process <|-- WorkProcess
Property <|-- Device
ComponentProperty <|-- LinkOfComponent
Subtask <|-- WorkFlowTask
Property <|-- Joint

@enduml
```
